---
layout: markdown_page
title: "Security Controls RACI Chart"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## RACI Overview

RACI charts break down the ownership various elements into the following categories:
* Responsible
  * This is the team that performs the work relating to the related control
* Accountable
  * This is the team required to answer for the final result of the control when audited
* Consulted
  * This is the team that has the right and ability to provide feedback relating to the control
* Informed
  * This is the team that will get an "FYI" relating to the control
    * It is worth noting here that since the majority of information at GitLab is publicly viewable, this refers to explicit notification of control information since a broader definition would have every GitLab team marked as "Informed"

### Security Controls RACI Chart

The below chart shows ownership of the [GitLab security controls](./sec-controls.html) as the compliance team views it today. This information will be updated as we work the various GitLab teams and learn more about how each control operates within the company.

Please do not edit this page directly since the compliance team uses this information to track control ownership. If you notice any errors or have any feedback on this chart, please comment on [this issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/217).

For additional information about the below controls, see the [security controls page](./sec-controls.html).

| Control Number | Control Short Name | Common Control Activity | Product Teams | People Ops Team | Legal Team | Security Team | Compliance Team | IT Ops /Business OpsTeam | Infrastructure Team |
| :----: | :----: | :----: | :----: | :----: | :----: | :----: | :----: | :----: | :----: |
| BC.1.01 | Business Continuity Plan | GitLab's business contingency plan is reviewed, approved by management and communicated to relevant team members quarterly. | R | R | I | A | C | R | R |
| BC.1.03 | Continuity Testing | GitLab performs business contingency and disaster recovery tests quarterly and ensures the following: * tests are executed with relevant contingency teams * test results are documented * corrective actions are taken for exceptions noted * plans are updated based on results | I |  |  | A | R | R | R |
| BC.1.04 | Business Impact Analysis | GitLab identifies the business impact of relevant threats to assets, infrastructure, and resources that support critical business functions. Recovery objectives are established for critical business functions. | C | C | C | A | R | C | C |
| BU.1.01 | Backup Configuration | GitLab configures redundant systems or performs data backups quarterly to resume system operations in the event of a system failure. | I |  |  | C | A | R | R |
| BU.1.02 | Resilience Testing | GitLab performs backup restoration or failover tests quarterly to confirm the reliability and integrity of system backups or recovery operations. | I |  |  | C | A | R | R |
| CM.1.01 | Change Management Workflow | Change scope, change type, and roles and responsibilities are pre-established and documented in a change control workflow; notification and approval requirements are also pre-established based on risk associated with change scope and type. | I |  |  | A | R | I | C |
| CM.1.02 | Change Approval | Prior to introducing changes into the production environment, approval from authorized personnel is required based on the following:  * change description * impact of change * test results * back-out procedures | R |  |  | A | C | R | R |
| CON.1.01 | Baseline Configuration Standard | GitLab ensures security hardening and baseline configuration standards have been established according to industry standards and are reviewed and updated quarterly. | I |  |  | C | A | R | R |
| CON.1.03 | Configuration Checks | GitLab uses mechanisms to detect deviations from baseline configurations in production environments. | I |  |  | C | A | I | R |
| CON.1.04 | Configuration Check Reconciliation: CMDB | GitLab reconciles the established device inventory against the enterprise log repository quarterly; devices which do not forward security configurations are remediated. | I |  |  | A | R | I | R |
| CON.1.07 | Default Device Passwords | Vendor-supplied default passwords are changed according to GitLab standards prior to device installation on the GitLab network or immediately after software or operating system installation. |  |  |  | C | A | R | R |
| DM.2.01 | Terms of Service | Consent is obtained for GitLab's Terms of Service (ToS) prior to collecting personal information and when the ToS is updated. | I | I | R | I | A | I | I |
| DM.4.01 | Encryption of Data in Transit | Red, orange, and yellow data that is transmitted over public networks is encrypted. | I |  |  | C | A | I | R |
| DM.7.01 | Secure Disposal of Media | GitLab securely erases media containing decommissioned red and orange data and obtains a certificate or log of erasure; media pending erasure are stored within a secured facility. |  |  |  | C | A |  | R |
| IAM.1.01 | Logical Access Provisioning | Logical access provisioning to information systems requires approval from appropriate personnel. | I | R |  | I | A | R | R |
| IAM.1.02 | Logical Access De-provisioning | Logical access that is no longer required in the event of a termination is documented, communicated to management, and revoked. | I | R |  | I | A | R | R |
| IAM.1.04 | Logical Access Review | GitLab performs account and access reviews quarterly; corrective action is taken where applicable. | I | C |  | A | R | C | C |
| IAM.1.06 | Shared Logical Accounts | GitLab restricts the use of shared and group authentication credentials. Authentication credentials for shared and group accounts are reset quarterly. | I | R |  | C | A | R | R |
| IAM.1.07 | Shared Account Restrictions | Where applicable, the use of generic and shared accounts to administer systems or perform critical functions is prohibited; generic user IDs are disabled or removed. |  | R |  | C | A | R | R |
| IAM.2.01 | Unique Identifiers | GitLab requires unique identifiers for user accounts and prevents identifier reuse. |  | R |  | C | A | R | R |
| IAM.2.02 | Password Authentication | User and device authentication to information systems is protected by passwords that meet GitLab's password complexity requirements. GitLab requires system users to change passwords quarterly. |  | R |  | C | A | R | R |
| IAM.3.02 | Source Code Security | Access to modify source code is restricted to authorized personnel. | R |  |  | C | A | R | R |
| IAM.4.01 | Virtual Private Network | Remote connections to the corporate network are accessed via VPN through managed gateways. |  | I |  | A | C | R | R |
| IAM.6.01 | Key Repository Access | Access to the cryptographic keystores is limited to authorized personnel. | I |  |  | A | C | R | R |
| IR.1.01 | Incident Response Plan | GitLab defines the types of incidents that need to be managed, tracked and reported, including:  * procedures for the identification and management of incidents * procedures for the resolution of confirmed incidents * key incident response systems * incident coordination and communication strategy * contact method for internal parties to report incidents * support team contact information * notification to relevant management in the event of a security breach * provisions for updating and communicating the plan * provisions for training of support team * preservation of incident information * management review and approval, quarterly, or when major changes to the organization occur | I | I | I | A | C | R | R |
| IR.1.03 | Incident Response | Confirmed incidents are assigned a priority level and managed to resolution. If applicable, GitLab coordinates the incident response with business contingency activities. | I | I | I | A | C | R | R |
| IR.2.01 | External Communication of Incidents | GitLab defines external communication requirements for incidents, including: * information about external party dependencies * criteria for notification to external parties as required by GitLab policy in the event of a security breach * contact information for authorities (e.g., law enforcement, regulatory bodies, etc.) * provisions for updating and communicating external communication requirement changes | I | I | I | A | R | C | C |
| IR.2.02 | Incident Reporting Contact Information | GitLab provides a contact method for external parties to: * submit complaints and inquiries * report incidents | I | I | C | R | A | I | I |
| IR.2.03 | Incident External Communication | GitLab communicates a response to external stakeholders as required by the Incident Response Plan. | I | I | C | R | A | I | I |
| NO.1.01 | Network Policy Enforcement Points | Network traffic to and from untrusted networks passes through a policy enforcement point; firewall rules are established in accordance to identified security requirements and business justifications. | I |  |  | A | C | R | R |
| PR.1.01 | Background Checks | New hires are required to pass a background check as a condition of their employment. |  | R | C |  | A |  |  |
| PR.1.02 | Performance Management | GitLab has established a check-in performance management process for on-going dialogue between managers and team members. quarterly reminders are sent to managers to perform their regular check-in conversation. | R | A | C | R | R | R | R |
| RM.1.01 | Risk Assessment | GitLab management performs a risk assessment quarterly. Results from risk assessment activities are reviewed to prioritize mitigation of identified risks. | R | R | C | R | A | R | R |
| RM.1.02 | Continuous Monitoring | The design and operating effectiveness of internal controls are continuously evaluated against the established common controls framework by GitLab. Corrective actions related to identified deficiencies are tracked to resolution. |  | C |  | A | R | C | C |
| RM.1.04 | Service Risk Rating Assignment | Quarterly, GitLab prioritizes the frequency of vulnerability discovery activities based on an assigned service risk rating. | I | I | I | C | A | R | R |
| RM.2.01 | Internal Audits | GitLab establishes internal audit requirements and executes audits on information systems and processes quarterly. | I | I | C | A | R | I | I |
| RM.3.01 | Remediation Tracking | Management prepares a remediation plan to formally manage the resolution of findings identified in risk assessment activities. | R | R | R | R | A | R | R |
| SDM.1.01 | System Documentation | Documentation of system boundaries and key aspects of their functionality are published to authorized personnel. | I |  |  | C | A | R | R |
| SDM.2.01 | Whitepapers | GitLab publishes whitepapers to its public website that describe the purpose, design, and boundaries of the system and system components. |  |  |  | C | A | C | R |
| SG.1.01 | Policy and Standard Review | GitLab's policies and standards are reviewed, approved by management, and communicated to authorized personnel quarterly. | I | I | I | A | R | C | C |
| SG.2.01 | Information Security Program Content | The GitLab Director of Security conducts a periodic staff meeting to communicate and align on relevant security threats, program performance, and resource prioritization. | I | I | I | R | A | I | I |
| SG.5.03 | Security Roles and Responsibilities | Roles and responsibilities for the governance of Information Security within GitLab are formally documented within the Information Security Management Standard and communicated on the GitLab intranet. | I | I | I | A | R | I | I |
| SLC.1.01 | Service Lifecycle Workflow | Major software releases are subject to the Service Life Cycle, which requires acceptance via Concept Accept and Project Plan Commit phases prior to implementation. | R |  |  | C | A | R | C |
| SYS.1.01 | Audit Logging | GitLab logs critical information system activity. |  |  |  | I | A | R | R |
| SYS.1.06 | Log Reconciliation: CMDB | GitLab reconciles the established device inventory against the enterprise log repository quarterly; devices which do not forward log data are remediated. |  |  |  | A | R | R | R |
| SYS.2.01 | Security Monitoring Alert Criteria | GitLab defines security monitoring alert criteria, how alert criteria will be flagged, and identifies authorized personnel for flagged system alerts. | I |  |  | C | A | C | C |
| SYS.2.07 | System Security Monitoring | Critical systems are monitored in accordance to predefined security criteria and alerts are sent to authorized personnel. Confirmed incidents are tracked to resolution. | I |  |  | C | A | C | C |
| SYS.3.01 | Availability Monitoring Alert Criteria | GitLab defines availability monitoring alert criteria, how alert criteria will be flagged, and identifies authorized personnel for flagged system alerts. | I |  |  | A | R | I | C |
| SYS.3.02 | System Availability Monitoring | Critical systems are monitored in accordance to predefined availability criteria and alerts are sent to authorized personnel. | I |  |  | C | A | I | R |
| TPM.1.01 | Third Party Assurance Review | Quarterly, management reviews controls within third party assurance reports to ensure that they meet organizational requirements; if control gaps are identified in the assurance reports, management takes action to address impact the disclosed gaps have on the organization. |  |  | I | A | R |  |  |
| TPM.1.02 | Vendor Risk Management | GitLab performs a risk assessment to determine the data types that can be shared with a managed service provider. |  |  | I | A | R |  |  |
| TPM.1.04 | Vendor Compliance Monitoring | Maintain a program to monitor service providers’ compliance status at least annually. |  |  | I | A | R |  |  |
| TPM.2.02 | Vendor Non-disclosure Agreements | Requirements for confidentiality or non-disclosure agreements reflecting the organization’s needs for the protection of information shall be identified, regularly reviewed and documented. |  |  | C | A | R |  |  |
| TPM.3.01 | Approved Service Provider Listing | GitLab maintains a list of approved managed service providers and the services they provide to GitLab. |  |  | I | A | R |  |  |
| TRN.1.01 | General Security Awareness Training | All GitLabbers complete security awareness training, which includes updates about relevant policies and how to report security events to the authorized response team. Records of training completion are documented and retained for tracking purposes. | R | R | C | R | A | R | R |
| TRN.1.02 | Code of Conduct Training | All GitLabbers complete a code of business conduct training. | R | R | C | R | A | R | R |
| VUL.1.01 | Vulnerability Scans | GitLab conducts vulnerability scans against the production environment; scan tools are updated prior to running scans. | I |  |  | A | C | I | R |
| VUL.2.01 | Application Penetration Testing | GitLab conducts penetration tests according to the service risk rating assignment. | I |  |  | A | C | I | R |
| VUL.3.01 | Infrastructure Patch Management | GitLab installs security-relevant patches, including software or firmware updates; identified end-of-life software must have a documented decommission plan in place before the software is removed from the environment. | I |  |  | A | C | I | R |
| VUL.4.01 | Enterprise Antivirus | If applicable, GitLab has managed enterprise antivirus deployments and ensures the following: * signature definitions are updated * full scans are performed quarterly and real-time scans are enabled * alerts are reviewed and resolved by authorized personnel | I |  |  | A | C | R | R |
| VUL.5.01 | Code Security Check | Quarterly, GitLab conducts source code checks for vulnerabilities according to the service risk rating assignment. | I |  |  | A | C | I | C |
| VUL.6.01 | External Information Security Inquiries | GitLab reviews information-security-related inquiries, complaints, and disputes. | I |  | I | A | R |  |  |
| VUL.7.01 | Vulnerability Remediation | GitLab assigns a risk rating to identified vulnerabilities and prioritizes remediation of legitimate vulnerabilities according to the assigned risk. | I |  |  | A | C |  | I |
